//
// Created by sc18gg on 2/8/20.
//


using namespace std;

#include "Lexer.h"
#include "Token.h"

#include <iostream>
#include <string>

#include <stdio.h>
#include <ctype.h>

/**
 * This file contains all the functions used by the lexer.
 */

// Reads the provided input file and produces a vector of characters.
void Lexer::ReadInput(const string& filename) {

    // checking file extension
    if (!(filename.substr(filename.find_last_of('.') + 1) == "jack")) {
        cout << "Error: file format not recognised\n";
        exit(EXIT_FAILURE);
    }

    ifstream is(filename);

    if (!is.is_open()) {
        cout << "File " << filename << " not found! Make sure you put it in the same folder as compiler executable.\n";
        exit(EXIT_FAILURE);
    }

    char nextChar;

    char after;

    char after2;

    int line = 1, lineNext = 1;

    // The following code removes single line double slash comments
    // As well as single and multi line /* */ type comments.
    while (is.get(nextChar)) {

        if (nextChar == '/') {
            is.get(after);

            // If the next character is end of file, push it to the input list and stop reading.
            if (is.eof()) {
                inputList.push_back(nextChar);
                break;
            }
            // Start of a single line double slash comment.
            if (after == '/') {
                while (is.get(after)) {
                    if (after == '\n') {
                        break;
                    }
                }
                if (!is.eof() && after == '\n') {
                    ++line;
                    // Add a new line instead of a comment.
                    inputList.push_back('\n');
                    continue;
                }
            }
            // Start of a multi-line comment.
            else if (after == '*') {

                // Copy of the line number required in case of unexpected reach of end of file.
                lineNext = line;
                while (is.get(after)) {
                    if (after == '*') {
                        is.get(after2);
                        if (is.eof()) {
                            cout << "Error, line: " << line << " Missing closing comment characters\n";
                            exit(EXIT_FAILURE);
                        }
                        // End of a multi-line comment.
                        if (after2 == '/') {
                            // Adds an empty space instead of a comment.
                            inputList.push_back(' ');
                            break;
                        }
                    }
                    else if (after == '\n') {
                        ++lineNext;
                        // Adds a new line whenever a new line is read during a multi-line comment
                        // So that errors are displayed correctly.
                        inputList.push_back('\n');
                    }
                }

                if (is.eof()) {
                    cout << "Error, line: " << line << " Missing closing comment characters\n";
                    exit(EXIT_FAILURE);
                }
                // If end of file is not reached during the multi-line comment, update the line number.
                else {
                    line = lineNext;
                }
            }

            else {
                if (after == '\n') {
                    ++line;
                }
                inputList.push_back(nextChar);
                inputList.push_back(after);
            }
        }
        else {
            if (nextChar == '\n') {
                ++line;
            }
            else if (nextChar == '\r') {
                continue;
            }
            inputList.push_back(nextChar);
        }

    }

    if (is.eof()) {
        inputList.push_back(char_traits<char>::eof());
    }
    else {
        cout << "Error reading the file\n";
    }

    is.close();
}

// Reads the input vector and produces tokens according to the jack language rules.
void Lexer::TokenizeAll() {

    string lexeme;

    // i points  at the next character
    // j points at the same character as i initially and is incremented whenever there is a multi-character token.
    int i = 0, j = 0, line = 1;

    vector<string> keywords = {"class", "constructor", "method", "function", "int",
                "boolean", "char", "void", "var", "static",
                "field", "let", "do", "if", "else",
                "while", "return", "true", "false", "null", "this"};

    vector<char> symbols = {'(', ')', '[', ']', '{', '}', ',', ';', '=','.',
                            '+', '-', '*', '/', '&', '|', '~', '<', '>', };

    while(i < inputList.size()) {

        if (inputList.at(i) == '\n') {
            Tokens.push_back(new Token("\n", Token::new_line));
            ++line;
            ++i;
        }
        // Start of a string literal.
        else if (inputList.at(i) == '"') {
            j = i + 1;
            // Read the string further.
            while (inputList.at(j) != '"' && inputList.at(j) != '\n' && inputList.at(j) != char_traits<char>::eof()) {
                lexeme += inputList.at(j);
                ++j;
            }
            // End of the string literal, create a new token from the resulting string.
            if (inputList.at(j) == '"') {
                Tokens.push_back(new Token(lexeme, Token::string_literal));
                lexeme = "";
                i = j + 1;
                j = i;
            }
            // End of line is reached, unexpectedly.
            else if (inputList.at(j) == '\n' || inputList.at(j) != char_traits<char>::eof()) {
                cout << "Error, line: " << line << " Missing closing quotes\n";
                exit(EXIT_FAILURE);
            }
        }
        // Start of either a keyword or an identifier.
        else if (isalpha(inputList.at(i)) || inputList.at(i) == '_') {
            j = i + 1;
            lexeme += inputList.at(i);

            bool checkForKeyword = true;

            // Keywords can not contain '_' character.
            if (inputList.at(i) == '_') {
                checkForKeyword = false;
            }

            // Read the token further.
            while (isalpha(inputList.at(j)) || isdigit(inputList.at(j)) || inputList.at(j) == '_') {

                // Keywords can not contain '_' character as well as digits.
                if (inputList.at(j) == '_' || isdigit(inputList.at(j))) {
                    checkForKeyword = false;
                }

                lexeme += inputList.at(j);
                ++j;
            }
            if (inputList.at(j) == '\n') {
                Tokens.push_back(new Token("\n", Token::new_line));
                ++line;
                i = j + 1;
                j = i;
            }
            // Advance the reading to the character after a tab or a space.
            else if (inputList.at(j) == ' ' || inputList.at(j) == '\t'){
                i = j + 1;
            }
            else {
                i = j;
            }

            bool keywordTrue = false;

            // Performs a check on keywords and add a keyword token if it is a keyword.
            if (checkForKeyword) {
                for (int a = 0; a < keywords.size(); a++) {
                    if (lexeme == keywords.at(a)) {
                        Tokens.push_back(new Token(lexeme, Token::keyword));
                        keywordTrue = true;
                        lexeme = "";
                        break;
                    }
                }
            }

            // If the token is not a keyword, then it is an identifier.
            if (!keywordTrue) {
                Tokens.push_back(new Token(lexeme, Token::id));
                Tokens.back()->setLineNumber(line);
                lexeme = "";
            }
        }

        // Start of a number, check for consequent integers.
        else if (isdigit(inputList.at(i))) {
            j = i + 1;
            lexeme += inputList.at(i);
            while (isdigit(inputList.at(j))) {
                lexeme += inputList.at(j);
                ++j;
            }
            if (inputList.at(j) == '\n') {
                Tokens.push_back(new Token("\n", Token::new_line));
                ++line;
                i = j + 1;
                j = i;
            }
            else {
                i = j;
            }

            Tokens.push_back(new Token(lexeme, Token::number));
            lexeme = "";
        }

        else {

            if (inputList.at(i) == '\n') {
                Tokens.push_back(new Token("\n", Token::new_line));
                ++line;
                ++i;
            }

            else if (inputList.at(i) == ' ' || inputList.at(i) == '\t') {
                ++i;
            }

            // Create an end of file token.
            else if (inputList.at(i) == char_traits<char>::eof()) {
                lexeme = inputList.at(i);
                Tokens.push_back(new Token(lexeme, Token::eof));
                lexeme = "";
                ++i;
            }

            else {

                bool allowedSymbol = false;

                for (int b = 0; b < symbols.size(); b++) {
                    if (inputList.at(i) == symbols.at(b)) {
                        allowedSymbol = true;
                    }
                }

                // If a symbol is as of Jack grammar, then create the corresponding token.
                if (allowedSymbol) {
                    lexeme = inputList.at(i);
                    Tokens.push_back(new Token(lexeme, Token::symbol));
                    lexeme = "";
                    ++i;
                }
                else {
                    cout << "Error, line: " << line << " Unknown symbol entered"<< "\n";
                    exit(EXIT_FAILURE);
                }
            }
        }

    }
}

// Gets next token from the input file and removes it from the list.
Token* Lexer::GetNextToken() {
    auto *frontToken = Tokens.front();

    Tokens.erase(Tokens.begin());

    return frontToken;
}

// Gets next token from the input file without removing it from the list.
Token* Lexer::PeekNextToken() {
    return Tokens.front();
}

// Gets a token at next token + extra from the input file without removing it from the list.
Token* Lexer::PeekNextTokenPlus(int extra) {
    return Tokens.at(extra);
}

// Checks if the Tokens list is empty.
bool Lexer::TokenEmpty() {
    return Tokens.empty();
}