//
// Created by sc18gg on 2/8/20.
//

#ifndef JACK_COMPILER_TOKEN_H
#define JACK_COMPILER_TOKEN_H

#include <string>
#include <vector>

/**
 * This class represents a token which is a single piece of information in a Jack file. It is used in the Parser
 * analysis later.
 */

using namespace std;

class Token {

public:
    enum TokenTypes {keyword, id, symbol, number, string_literal, eof, new_line};

    string lexeme;

    TokenTypes type;

    Token() = default;;

    Token(const string &lexeme, TokenTypes type);

    const string &getLexeme() const;

    void setLexeme(const string &lexeme);

    int getLineNumber() const;

    void setLineNumber(int lineNumber);

    TokenTypes getType() const;

    void setType(TokenTypes type);

private:

    int lineNumber;

};


#endif //JACK_COMPILER_TOKEN_H
