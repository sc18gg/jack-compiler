#include <iostream>
#include "Lexer.h"
#include "Parser.h"
#include "Symbol.h"

#include <string>
#include <list>

/**
 * This is the main file of the Jack Compiller written by Georgy Gunkin (sc18gg).
 *
 * It compiles the files given to it via command line arguments one by one,
 * until an exception is raised or until it finishes compiling every file.
 *
 * If an exception is raised, it recompiles the whole program with the files switched between each other.
 */

using namespace std;

// Extracts the file name from a string.
// Assuming delimenter is '/'
string findFileName(string str) {

    size_t pos = 0;
    string token;
    while ((pos = str.find('/')) != string::npos || !str.empty()) {

        if (pos == string::npos) {
            return str;
        }

        token = str.substr(0, pos);
        str.erase(0, pos + 1);
    }

    return token;
}

// Extracts the class name from a string.
string findClassName(string str) {
    int pos = 0;
    for (int i = 0; i < str.size(); ++i) {
        if (str.at(i) == '.') {
            pos = i;
            break;
        }
    }
    return str.substr(0, pos);
}

// Extracts the type of the function which caused exception.
string findType(string str) {

    size_t pos = 0;
    int tokenNum = 0;
    string token;
    while (((pos = str.find(' ')) != string::npos || !str.empty()) && tokenNum != 4) {

        if (pos == string::npos) {
            return str;
        }

        token = str.substr(0, pos);
        str.erase(0, pos + 1);

        ++tokenNum;
    }

    return token;
}

// Extracts the name of the class or function which caused exception.
string findClassOrFunction(string str) {
    size_t pos = 0;
    int tokenNum = 0;
    string token;
    while (((pos = str.find(' ')) != string::npos || !str.empty()) && tokenNum != 3) {

        if (pos == string::npos) {
            return str;
        }

        token = str.substr(0, pos);
        str.erase(0, pos + 1);

        ++tokenNum;
    }

    return token;
}

int main(int argc, char **argv) {

    vector<string> sources;

    for (int i = 1; i < argc; ++i) {
        sources.emplace_back(argv[i]);
    }

    vector<string>::const_iterator iterator = sources.begin();

    Parser *parser;

    string filePath;

    vector<string> vmFilesToClean;

    if (argc <= 1) {
        cout << "Not enough files to compile! Must be 1 or more" << endl;
        return 1;
    }

    // + 8 built in classes.
    int numberOfClasses = argc - 1 + 8;
    int numberOfTries = 0;
    int size = sources.size();
    int currentFile = 0;

    bool successfulCompile = false;

    // Tries to compile a given number of times which depends on the number of files required to be compiled.
    while (numberOfTries <= numberOfClasses && !successfulCompile) {

        try {

            for (int i = 0; i < size; ++i, ++currentFile) {

                filePath = sources.at(i);

                string fileName = findFileName(filePath);

                string requiredClassName = findClassName(fileName);

                string vmFileName = requiredClassName + ".vm";

                vmFilesToClean.push_back(vmFileName);

                // Initialising the program table.
                if (Parser::currentProgramState.size() < size) {
                    Parser::currentProgramState.emplace_back();
                }

                cout << endl << "Compiling " << fileName << endl;

                // Passing i means that this parser will operate on the i'th position of the program table.
                parser = new Parser(filePath, numberOfClasses, i, requiredClassName, vmFileName);

                parser->runLexer();

                parser->classDeclar();

                cout << endl << "Successfully compiled " << fileName << endl;
            }
            successfulCompile = true;

            cout << endl << "Successfully compiled the program" << endl;
        }

        // Exception occurs whenever a class or function is not found. It may be the case that it was not seen yet.
        // The code below resolves it. It reloads the compilation by switching the files between each other.
        catch (runtime_error &r) {

            cout << "Exception: " << r.what() << endl;

            string classType = findType(r.what());

            string class_or_function = findClassOrFunction(r.what());

            string className;

            bool classExists = false;

            for (int k = 0; k < sources.size(); ++k) {

                className = findClassName(findFileName(sources.at(k)));
                if (classType == className) {

                    // Means that the class will be compiled later on. (Otherwise a non-existent class has been used.)
                    classExists = true;
                }
            }

            if (!classExists) {
                if (class_or_function == "type:") {
                    cout << "Class (" << classType << ") is used, however its declaration was not found" << endl;
                    exit(EXIT_FAILURE);
                }
                else if (class_or_function == "function:") {
                    cout << "Function (" << classType << ") is used, however its declaration was not found" << endl;
                    exit(EXIT_FAILURE);
                }
            }

            // Changes the order of the classes to be parsed to avoid using a non-declared yet class.

            vector<string> sources_changed;

            ++numberOfTries;

            for (int i = 0; i < sources.size(); ++i) {
                if (i != currentFile) {
                    sources_changed.push_back(sources.at(i));
                }
            }
            sources_changed.push_back(sources.at(currentFile));
            sources.clear();

            for (int j = 0; j < sources_changed.size(); ++j) {
                sources.push_back(sources_changed.at(j));
            }

            currentFile = 0;
            parser->clearProgramTable();

            // Cleaning files from the assembly code written before the excpetion has been caught.
            for (int l = 0; l < vmFilesToClean.size(); ++l) {
                ofstream cleanFile;
                cleanFile.open(vmFilesToClean.at(l), ios::trunc);
                cleanFile.close();
            }
        }
    }

    return 0;
}


