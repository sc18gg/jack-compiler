//
// Created by sc18gg on 4/22/20.
//

#ifndef JACK_COMPILER_SYMBOL_H
#define JACK_COMPILER_SYMBOL_H

#include <string>
#include <vector>

/**
 * This files represents a symbol class which is used to populate the symbol tables.
 */

using namespace std;

class Symbol {

public:

    Symbol(const string &name, const string &type, const string &kind, int number) {
        this->name = name;
        this->type = type;
        this->kind = kind;
        this->position = number;
        this->initialised = false;
    }

    // For arguments.
    Symbol(const string &name, const string &type, const string &kind, int number, bool init) {
        this->name = name;
        this->type = type;
        this->kind = kind;
        this->position = number;
        this->initialised = init;
    }

    const string &getName() const {
        return name;
    }

    const string &getType() const {
        return type;
    }

    const string &getKind() const {
        return kind;
    }

    int getPosition() const {
        return position;
    }

    bool getInitialised() const {
        return initialised;
    }

    void setInitialised() {
        initialised = true;
    }

    void addArgumentType(const string &argT) {
        argumentTypes.push_back(argT);
    }

    vector<string> getArguments() {
        return argumentTypes;
    }

    int getNumberArguments() {
        return argumentTypes.size();
    }

private:

    string name, type, kind;

    bool initialised;

    int position;

    vector<string> argumentTypes;

};

#endif //JACK_COMPILER_SYMBOL_H
