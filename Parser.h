//
// Created by sc18gg on 2/29/20.
//

#ifndef JACK_COMPILER_PARSER_H
#define JACK_COMPILER_PARSER_H

#include <string>
#include <list>
#include <deque>
#include "Lexer.h"
#include "Token.h"
#include "Symbol.h"
#include <fstream>
#include <vector>
#include <exception>
#include <bits/stdc++.h>

/**
 * The roles of this class is to run a lexer on a file, parse it, perform all the required
 * semantic checks and generate the VM code for the Virtual Machine to run it.
 */

class Parser {

public:

    // Static program table. Consists of a vector of symbol tables: one for each file.
    static vector<vector<vector<Symbol*>>> currentProgramState;

    int line;
    int currentTable;
    int functionHasReturn;
    int returnInAllPaths;
    int inMeaningfulIf;
    int numberOfClassVars;

    int thisClassPosition;

    bool hasReturn;
    bool inConstructor;

    bool inSubroutineCall;

    // Constructor for the parser.
    Parser(string fName, int num, int pos, string fCName, const string& vmFileName) {
        line = 1;
        // Creates a global table.
        thisClassPosition = pos;

        if (currentProgramState.at(thisClassPosition).empty()) {
            currentProgramState.at(thisClassPosition).emplace_back();
        }

        // Adds other classes to the current class table.
        addOtherClassesToTable();

        hasReturn = false;
        inSubroutineCall = false;
        inConstructor = false;

        currentFunctionSymbol = nullptr;

        // Open vm file for write. It will overwrite the file if such exists.
        vmf.open(vmFileName, ios::trunc);

        numberOfClassVars = 0;
        countWhiles = -1;
        countIfs = -1;
        currentTable = 0;
        functionHasReturn = -1;
        returnInAllPaths = -1;
        inMeaningfulIf = -1;
        fileName = fName;
        fileClassName = fCName;
        classesNumber = num;
    }

    ofstream vmf;

    Symbol *currentFunctionSymbol;

    vector<int> numberWhiles;
    int countWhiles;

    vector<int> numberIfs;
    int countIfs;

    string className;
    string fileName;
    string fileClassName;
    int classesNumber;

    // Parser contains Lexer.
    Lexer *lexer = new Lexer();

    // Runs lexer on a given file.
    void runLexer() {
        lexer->ReadInput(fileName);
        lexer->TokenizeAll();
    }

    // Clears the program table by clearing every file's symbol table.
    void clearProgramTable() {
        currentProgramState.clear();
        currentProgramState = {};
    }

    // Checks if a given string is an operator in Jack Language.
    bool checkIfOperator(const string &str) {
        return (str == "+" || str == "-" || str == "*" ||
               str == "/" || str == "&" || str == "|" ||
               str == "~" || str == "<" || str == ">" || str == "=");
    }

    // Outputs the corresponding VM command for the operator.
    void outputOperator(const string &op) {
        if (op == "+") {
            vmf << "add" << endl;
        }
        else if (op == "-") {
            vmf << "sub" << endl;
        }
        else if (op == "*") {
            vmf << "call Math.multiply 2" << endl;
        }
        else if (op == "/") {
            vmf << "call Math.divide 2" << endl;
        }
        else if (op == "&") {
            vmf << "and" << endl;
        }
        else if (op == "|") {
            vmf << "or" << endl;
        }
        else if (op == "~") {
            vmf << "not" << endl;
        }
        else if (op == "<") {
            vmf << "lt" << endl;
        }
        else if (op == ">") {
            vmf << "gt" << endl;
        }
        else if (op == "=") {
            vmf << "eq" << endl;
        }
    }

    // This function resolves an expression given and populates VM stack with corresponding VM commands.
    void resolveExpression(vector<Token*> expression);


    // Checks if the given function is declared later in the file.
    bool checkIfFunctionDeclared(const string &function_name) {
        Token *nextToken = lexer->PeekNextToken();
        int i = 0;

        while (nextToken->getType() != Token::TokenTypes::eof) {
            if ((nextToken->getLexeme() == "function" || nextToken->getLexeme() == "method") &&
                 nextToken->getType() == Token::TokenTypes::keyword) {

                if (((i+2) < lexer->getTokensSize()) && lexer->PeekNextTokenPlus(i+2)->getLexeme() == function_name &&
                    lexer->PeekNextTokenPlus(i+2)->getType() == Token::TokenTypes::id) {

                    return true;
                }
            }
            ++i;
            nextToken = lexer->PeekNextTokenPlus(i);
        }
        return false;
    }

    // Given a function which has not yet been encountered as declared, it finds and returns its type.
    string findUndeclaredFunctionType(const string &function_name) {
        Token *nextToken = lexer->PeekNextToken();
        int i = 0;

        while (nextToken->getType() != Token::TokenTypes::eof) {
            if ((nextToken->getLexeme() == "function" || nextToken->getLexeme() == "method") &&
                nextToken->getType() == Token::TokenTypes::keyword) {

                if (((i+2) < lexer->getTokensSize()) && lexer->PeekNextTokenPlus(i+2)->getLexeme() == function_name &&
                    lexer->PeekNextTokenPlus(i+2)->getType() == Token::TokenTypes::id) {

                    // Returning type of the function.
                    return lexer->PeekNextTokenPlus(i+1)->getLexeme();
                }
            }
            ++i;
            nextToken = lexer->PeekNextTokenPlus(i);
        }
        return "-1";
    }

    // Takes a vector of arguments and determines their types. Returns a vector of types.
    void extractArgumentsUndeclaredFunction(const string &function_name, vector<string> &types) {
        Token *nextToken = lexer->PeekNextToken();
        int i = 0;

        while (nextToken->getType() != Token::TokenTypes::eof) {
            if ((nextToken->getLexeme() == "function" || nextToken->getLexeme() == "method") &&
                nextToken->getType() == Token::TokenTypes::keyword) {

                // Declaration: function/method ... function_name(args) {
                if (((i+2) < lexer->getTokensSize()) && lexer->PeekNextTokenPlus(i+2)->getLexeme() == function_name &&
                    lexer->PeekNextTokenPlus(i+2)->getType() == Token::TokenTypes::id) {

                    // Start of arguments.
                    if (lexer->PeekNextTokenPlus(i+3)->getLexeme() == "(") {
                        int j = i + 4;
                        int count = 0;
                        while (j < lexer->getTokensSize() && lexer->PeekNextTokenPlus(j)->getLexeme() != ")") {
                            if (count == 0 || count%3 == 0) {

                                types.push_back(lexer->PeekNextTokenPlus(j)->getLexeme());
                            }
                            ++count;
                            ++j;
                        }
                    }
                }
            }
            ++i;

            nextToken = lexer->PeekNextTokenPlus(i);
        }
    }

    // Finds the position of the class in the program table.
    int findClassPosition(const string &class_name) {
        int sizeP = currentProgramState.size();
        int sizeS;

        for (int i = 0; i < sizeP; ++i) {
            if (!currentProgramState.at(i).empty()) {
                sizeS = currentProgramState.at(i).at(0).size();

                for (int j = 0; j < sizeS; ++j) {
                    if (currentProgramState.at(i).at(0).at(j)->getName() == class_name &&
                        currentProgramState.at(i).at(0).at(j)->getPosition() == 999) {

                        return i;
                    }
                }
            }
        }
        // Error.
        return -1;
    }

    // Finds a public symbol in class, that is the symbol declared in the class scope or in the global scope of the class.
    Symbol* findSymbolInClass(const string &name, int class_number) {
        int scope = 1;
        int sizeS;

        if (class_number < 0 || class_number >= currentProgramState.size()) {
            cout << "Line: " << line << "\n";
            cout << "Error: a symbol with name " << name << " does not exist\n";
            exit(EXIT_FAILURE);
        }
        else if (currentProgramState.at(class_number).empty()) {
            cout << "Line: " << line << "\n";
            cout << "Error: a symbol with name " << name << " does not exist\n";
            exit(EXIT_FAILURE);
        }
        else if (currentProgramState.at(class_number).size() == 1) {
            scope = 0;
        }

        while (scope >= 0) {
            sizeS = currentProgramState.at(class_number).at(scope).size();

            for (int i = 0; i < sizeS; ++i) {
                if (currentProgramState.at(class_number).at(scope).at(i)->getName() == name) {
                    return currentProgramState.at(class_number).at(scope).at(i);
                }
            }
            --scope;
        }

        cout << "Line: " << line << "\n";
        cout << "Error: a symbol with name " << name << " does not exist\n";
        exit(EXIT_FAILURE);
    }

    // Finds a public symbol in class, that is the symbol declared in the class scope or in the global scope of the class.
    Symbol* findSymbolInClassNoError(const string &name, int class_number) {
        int scope = 1;
        int sizeS;

        if (class_number < 0 || class_number >= currentProgramState.size()) {
            return new Symbol("not_found", "not_found", "not_found", 0);
        }
        else if (currentProgramState.at(class_number).empty()) {
            return new Symbol("not_found", "not_found", "not_found", 0);

        }
        else if (currentProgramState.at(class_number).size() == 1) {
            scope = 0;
        }

        while (scope >= 0) {
            sizeS = currentProgramState.at(class_number).at(scope).size();

            for (int i = 0; i < sizeS; ++i) {
                if (currentProgramState.at(class_number).at(scope).at(i)->getName() == name) {
                    return currentProgramState.at(class_number).at(scope).at(i);
                }
            }
            --scope;
        }

        return new Symbol("not_found", "not_found", "not_found", 0);
    }

    // Given a class number in the program table, it finds a function and outputs its Symbol.
    Symbol* findFunctionInClass(const string &function_name, int class_number) {
        // Functions and methods must be in the scope of a class.

        if (class_number < 0 || class_number >= currentProgramState.size()) {
            return new Symbol("not_found", "not_found", "not_found", 0);
        }
        else if (currentProgramState.empty()) {
            return new Symbol("not_found", "not_found", "not_found", 0);
        }
        else if (currentProgramState.at(class_number).empty()) {
            return new Symbol("not_found", "not_found", "not_found", 0);
        }

        int size = currentProgramState.at(class_number).at(1).size();

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(class_number).at(1).at(i)->getName() == function_name &&
                (currentProgramState.at(class_number).at(1).at(i)->getKind() == "function" ||
                 currentProgramState.at(class_number).at(1).at(i)->getKind() == "method" ||
                 currentProgramState.at(class_number).at(1).at(i)->getKind() == "constructor"))

                return currentProgramState.at(class_number).at(1).at(i);
        }

        return new Symbol("not_found", "not_found", "not_found", 0);
    }

    // Function which returns true if the current table is the main table of some function, and false otherwise.
    bool currentlyInFunctionScope() {
        int sizeS = currentProgramState.at(thisClassPosition).at(currentTable).size();

        for (int i = 0; i < sizeS; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getName() == "this") {
                return true;
            }
        }

        return false;
    }

    // Determines the number of functions a parser has seen so far. That number is the function position.
    int determineFunctionPosition() {

        // Signifies constructor.
        if (inConstructor) {
            return -10;
        }

        int thisCount = -1;
        int scope = currentTable;
        int sizeS;

        while (scope >= 0) {
            sizeS = currentProgramState.at(thisClassPosition).at(scope).size();

            for (int i = 0; i < sizeS; ++i) {
                if (currentProgramState.at(thisClassPosition).at(scope).at(i)->getName() == "this") {
                    ++thisCount;
                }
            }
            --scope;
        }

        if (classHasConstructor()) {
            if (thisCount != 0) {
                --thisCount;
            }
        }

        return thisCount;
    }

    // Checks if the current class has a constructor.
    bool classHasConstructor() {
        // In class scope.
        int sizeS = currentProgramState.at(thisClassPosition).at(1).size();

        for (int i = 0; i < sizeS; ++i) {
            if (currentProgramState.at(thisClassPosition).at(1).at(i)->getKind() == "constructor") {
                return true;
            }
        }
        return false;
    }

    // Given the number of functions the Parser has seen so far, determines the type of that last function.
    string determineFunctionType(int position) {
        const string error_type = "-1";

        // Constructor.
        if (position == -10) {
            return className;
        }

        const int class_scope = 1;

        int sizeS = currentProgramState.at(thisClassPosition).at(class_scope).size();

        for (int i = 0; i < sizeS; ++i) {
            if ((currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getKind() == "function" ||
                 currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getKind() == "method") &&
                currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getPosition() == position) {

                return currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getType();
            }
        }

        // Else return error.
        return error_type;
    }

    // Given the number of functions the Parser has seen so far, determines the kind of that last function.
    string determineFunctionKind(int position) {
        const string error_type = "-1";

        // Constructor.
        if (position == -10) {
            return "constructor";
        }

        const int class_scope = 1;

        int sizeS = currentProgramState.at(thisClassPosition).at(class_scope).size();

        for (int i = 0; i < sizeS; ++i) {
            if ((currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getKind() == "function" ||
                 currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getKind() == "method") &&
                currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getPosition() == position) {

                return currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getKind();
            }
        }

        // Else return error.
        return error_type;
    }

    // Given the number of functions the Parser has seen so far, determines the name of that last function.
    string determineFunctionName(int position) {

        const string error_name = "-1";

        // Constructor.
        if (position == -10) {
            return "new";
        }

        const int class_scope = 1;

        int sizeS = currentProgramState.at(thisClassPosition).at(class_scope).size();

        for (int i = 0; i < sizeS; ++i) {
                if ((currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getKind() == "function" ||
                     currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getKind() == "method") &&
                    currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getPosition() == position) {

                    return currentProgramState.at(thisClassPosition).at(class_scope).at(i)->getName();
                }
        }

        // Else return error.
        return error_name;
    }

    // Searches the current scope of the symbol table for a variable with the same name.
    void searchForRedeclaration(const string &name) {

        int sizeS = currentProgramState.at(thisClassPosition).at(currentTable).size();

        for (int j = 0; j < sizeS; ++j) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(j)->getName() == name) {
                cout << "Line: " << line << "\n";
                cout << "Error: a symbol with name " << name << " already exists\n";
                exit(EXIT_FAILURE);
            }
        }
    }

    // Adds a symbol to the table at the current scope.
    void addToTable(const string &name, const string &type, const string &kind) {
        int number = -1;

        if (kind == "static") {
            number = numberOfStatic();
        }
        else if (kind == "field") {
            number = numberOfField();
        }
        else if (kind == "argument") {
            number = numberOfArgument();
        }
        else if (kind == "var") {
            number = numberOfVar();
        }
        else if (kind == "constructor") {
            number = numberOfConstructors();
        }
        else if (kind == "function" || kind == "method") {
            number = numberOfFunctions();
        }
        else if (kind == "class") {
            number = numberOfClasses();
        }
        else if (kind == name) {
            number = 999;
        }

        if (number != 999) {
            ++number;
        }

        Symbol *newSymbol = new Symbol(name, type, kind, number);

        if (kind == "class") {
            addClassToProgramTable(newSymbol, name);
        }

        if (kind != "class") {
            currentProgramState.at(thisClassPosition).at(currentTable).push_back(newSymbol);
        }
    }

    // Adds the current class to every symbol table of every class in the program, so they can see it.
    void addClassToProgramTable(Symbol *new_class, const string &class_name) {
        // This is how many classes the compiler is to compile.
        int numClasses = currentProgramState.size();
        int remainder = classesNumber - numClasses;

        for (int i = 0; i < remainder; ++i) {
            currentProgramState.emplace_back();
        }

        for (int j = 0; j < classesNumber; ++j) {
            if (currentProgramState.at(j).empty()) {
                currentProgramState.at(j).emplace_back();
            }
            if (!classReDeclaration(currentProgramState.at(j), class_name)) {
                    currentProgramState.at(j).at(0).push_back(new_class);
            }
        }
    }

    // Checks if the given class has already been declared.
    bool classReDeclaration(vector<vector<Symbol*>> class_table, const string &class_name) {
        if (class_table.empty()) {
            return false;
        }

        int sizeG = class_table.at(0).size();

        for (int i = 0; i < sizeG; ++i) {
            if (class_table.at(0).at(i)->getName() == class_name) {
                return true;
            }
        }

        return false;
    }

    // For arguments: adds a symbol to the table at the current scope.
    void addToTable(const string &name, const string &type, const string &kind, const bool &init) {
        int number = -1;

        if (kind == "static") {
            number = numberOfStatic();
        }
        else if (kind == "field") {
            number = numberOfField();
        }
        else if (kind == "argument") {
            number = numberOfArgument();
        }
        else if (kind == "var") {
            number = numberOfVar();
        }
        else if (kind == "constructor") {
            number = numberOfConstructors();
        }
        else if (kind == "function" || kind == "method") {
            number = numberOfFunctions();
        }
        else if (kind == "class") {
            number = numberOfClasses();
        }

        ++number;

        Symbol *newArgSymbol = new Symbol(name, type, kind, number, init);

        currentProgramState.at(thisClassPosition).at(currentTable).push_back(newArgSymbol);
    }

    // Adds every single class the Parser has seen so far to the current Symbol table.
    void addOtherClassesToTable() {

        if (!currentProgramState.empty() && !currentProgramState.at(0).empty()) {
            int sizeP = currentProgramState.at(0).at(0).size();

            for (int i = 0; i < sizeP; ++i) {

                Symbol *new_class_symbol = currentProgramState.at(0).at(0).at(i);

                int sizeG = currentProgramState.at(thisClassPosition).at(0).size();

                bool classExists = false;

                // If such class is declared, skip.
                for (int j = 0; j < sizeG; ++j) {
                    if (currentProgramState.at(thisClassPosition).at(0).at(j)->getName() == new_class_symbol->getName()) {
                        classExists = true;
                        break;
                    }
                }

                if (!classExists) {
                    if (new_class_symbol->getPosition() != 999) {
                        currentProgramState.at(thisClassPosition).at(0).push_back(new_class_symbol);
                    }
                }

            }
        }
    }

    // Finds a symbol with the given name in the current function's scope and in the class scope.
    Symbol* findSymbol(const string &name) {
        int scope = currentTable;
        int sizeS;

        bool lastFunctionScope = false;

        while (scope >= 0) {
            sizeS = currentProgramState.at(thisClassPosition).at(scope).size();

            for (int i = 0; i < sizeS; ++i) {
                if (currentProgramState.at(thisClassPosition).at(scope).at(i)->getName() == name) {
                    return currentProgramState.at(thisClassPosition).at(scope).at(i);
                }
                else if (currentProgramState.at(thisClassPosition).at(scope).at(i)->getName() == "this") {
                    lastFunctionScope = true;
                }
            }

            if (!lastFunctionScope) {
                // Going to the parent scope.
                --scope;
            }
            else {
                // Going to the class scope.
                scope = 1;
                lastFunctionScope = false;
            }
        }

        cout << "Line: " << line << "\n";
        cout << "Error: a symbol with name " << name << " does not exist\n";
        exit(EXIT_FAILURE);
    }

    // Finds a symbol with the given name in the current function's scope and in the class scope, but does not
    // produce an error.
    Symbol* findSymbolNoError(const string &name) {
        int scope = currentTable;
        int sizeS;

        bool lastFunctionScope = false;

        while (scope >= 0) {
            sizeS = currentProgramState.at(thisClassPosition).at(scope).size();

            for (int i = 0; i < sizeS; ++i) {
                if (currentProgramState.at(thisClassPosition).at(scope).at(i)->getName() == name) {
                    return currentProgramState.at(thisClassPosition).at(scope).at(i);
                }
                else if (currentProgramState.at(thisClassPosition).at(scope).at(i)->getName() == "this") {
                    lastFunctionScope = true;
                }
            }

            if (!lastFunctionScope) {
                // Going to the parent scope.
                --scope;
            }
            else {
                // Going to the class scope.
                scope = 1;
                lastFunctionScope = false;
            }
        }

        // Return error symbol.
        return new Symbol("not_found", "not_found", "not_found", 0);
    }

    // Determines the number of static variables declared in the current class.
    int numberOfStatic() {
        int size = currentProgramState.at(thisClassPosition).at(currentTable).size();
        int count = -1;

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "static") {
                ++count;
            }
        }

        return count;
    }
    // Determines the number of field variables declared in the current class.
    int numberOfField() {
        int size = currentProgramState.at(thisClassPosition).at(currentTable).size();
        int count = -1;

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "field") {
                ++count;
            }
        }

        return count;
    }

    // Determines the number of argument variables declared in the current function.
    int numberOfArgument() {
        int size = currentProgramState.at(thisClassPosition).at(currentTable).size();
        int count = -1;

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "argument") {
                ++count;
            }
        }

        return count;
    }

    // Determines the number of var variables declared in the current function.
    int numberOfVar() {
        int size = currentProgramState.at(thisClassPosition).at(currentTable).size();
        int count = -1;

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "var") {
                ++count;
            }
        }

        return count;
    }

    // Determines the number of functions and methods declared in the current class.
    int numberOfFunctions() {
        int size = currentProgramState.at(thisClassPosition).at(currentTable).size();
        int count = -1;

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "function" ||
                currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "method") {

                ++count;
            }
        }

        return count;
    }

    // Determines the number of constructors declared in the current class.
    int numberOfConstructors() {
        int size = currentProgramState.at(thisClassPosition).at(currentTable).size();
        int count = -1;

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "constructor") {
                ++count;
            }
        }

        return count;
    }

    // Determines the number of classes present in the current symbol table.
    int numberOfClasses() {
        int size = currentProgramState.at(thisClassPosition).at(currentTable).size();
        int count = -1;

        for (int i = 0; i < size; ++i) {
            if (currentProgramState.at(thisClassPosition).at(currentTable).at(i)->getKind() == "class") {
                ++count;
            }
        }

        return count;
    }

    // The following functions have a corresponding explanation in the Parser.cpp file.

    void extractTypesOfArguments(const vector<vector<Token*>> &arguments, vector<string> &types);

    void classDeclar();

    void memberDeclar();

    void classVarDeclar();

    void type();

    void subroutineDeclar();

    void paramList();

    void subroutineBody();

    void statement();

    void varDeclarStatement();

    void letStatement();

    void ifStatement();

    void whileStatement();

    void doStatement();

    void subroutineCall();

    void expressionList();

    void returnStatement();

    void expression();

    void relationalExpression();

    void arithmeticExpression();

    void term();

    void factor();

    void operand();

};


#endif //JACK_COMPILER_PARSER_H
